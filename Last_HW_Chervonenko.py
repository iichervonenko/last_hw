# def readFileId(mode):
#     _ = ''
#     id = 0
#     for n in names:
#         with os.open(n, 'w') as f:
#             _ += f.read()
#         f.close()
#     print 'default: ' + id + ', actual: ' + _
#     return id ? _ : id


def read_file(mode):
    ide = 0
    name = ['test.txt', 'test1.txt', 'test2.txt']
    for n in name:
        with open(n, mode) as f:
            text = f.read()
            ide += 1
        print(f'Test: {ide}: {text}')
    return ide
read_file('r')

